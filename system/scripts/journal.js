/**
 * Extends the JournalEntity to process special things from L5R.
 */
export class JournalL5r5e extends JournalEntry {}
